# Terminal Customization
Terminal Customization is a collection of Customizations that you can do to your Terminal. \
All the Customizations can be found on the [Wiki](https://gitlab.com/Commandcracker/terminal-customization/-/wikis/home)
